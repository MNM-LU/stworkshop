FROM rocker/rstudio:3.3.2
#install latex for PDF output
RUN apt-get update
RUN apt-get install -y libxml2-dev libjpeg-dev libssh2-1-dev libgsl0-dev gsl-bin
ADD ./staligner /root/staligner
RUN Rscript -e "install.packages('devtools')"
RUN Rscript -e "library(devtools)" -e "install_version('jpeg',version = '0.1-8',repos = 'http://cran.us.r-project.org')" \
-e "install_version('rjson', version = '0.2.15', repos = 'http://cran.us.r-project.org')" \
-e "install_version('raster', version = '2.5-8', repos = 'http://cran.us.r-project.org')" \
-e "install_version('scales', version = '0.4.1', repos = 'http://cran.us.r-project.org')" \
-e "install_version('gdata', version = '2.17.0', repos = 'http://cran.us.r-project.org')" \
-e "install_version('igraph', version = '1.0.1', repos = 'http://cran.us.r-project.org')" \
-e "install_version('akima', version = '0.6-2', repos = 'http://cran.us.r-project.org')" \
-e "install_version('Rtsne', version = '0.13', repos = 'http://cran.us.r-project.org')" \
-e "install_version('SQUAREM', version = '2016.8-2', repos = 'http://cran.us.r-project.org')" \
-e "install_version('vioplot', version = '0.2', repos = 'http://cran.us.r-project.org')" \
-e "install_version('ggplot2', version = '2.2.1', repos = 'http://cran.us.r-project.org')" \
-e "install_version('gplots', version = '3.0.1', repos = 'http://cran.us.r-project.org')" \
-e "install_version('limSolve', version = '1.5.5.2', repos = 'http://cran.us.r-project.org')" \
-e "devtools::install('/root/staligner')" \
-e "devtools::install_github('klutometis/roxygen')"
RUN Rscript -e "source('https://bioconductor.org/biocLite.R')" -e "biocLite()"
RUN Rscript -e "source('https://bioconductor.org/biocLite.R')" -e "biocLite('org.Hs.eg.db')" \
-e "biocLite('org.Mm.eg.db')" \
-e "biocLite('org.Rn.eg.db ')" \
-e "biocLite('scran')" \
-e "biocLite('DOSE')" \
-e "biocLite('edgeR')" \
-e "biocLite('cellTree')" \
-e "biocLite('ReactomePA')" \
-e "devtools::install_github('kkdey/Countclust')"
RUN mkdir logs data output
RUN chown -R rstudio:rstudio /home/rstudio/*